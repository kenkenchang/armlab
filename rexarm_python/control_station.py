import sys
import cv2
import numpy as np
from scipy import signal 
from scipy import misc
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm
import ConfigParser
import time
from math import *
import matplotlib.pyplot as plt
import time


from video import Video

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510

class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))

        """ Other Variables """
        self.last_click = np.float32([0,0])
        self.workspace = []
        self.temp = []
        self.img = []
        self.tempimg = []
        self.imgsam = []
        self.tempimgsam = []
        self.temp_p =[]
        self.printpoints = []

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
       
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()

        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI (done)
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)
        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)
        

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnLoadPlan.clicked.connect(self.load_plan)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)
        
        """ flags """
        self.poly = 0       # smooth path for Cubic Polynomials
        self.fun = 0        # fun function
        self.defTemp = 0    # define template
        
        self.plan_start = -1 # execute not started
        self.now = -1


    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """

        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            self.video.drawSquare(self.printpoints)
            self.ui.videoFrame.setPixmap(
                self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"

        
        """ 
        Update GUI Joint Coordinates Labels 
        TO DO: include the other slider labels (done)
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))
        
        
        self.TIP = self.rex.rexarm_FK(-1)
        self.ui.rdoutX.setText(str(round(self.TIP[0],3)))
        self.ui.rdoutY.setText(str(round(self.TIP[1],3)))
        self.ui.rdoutZ.setText(str(round(self.TIP[2],3)))
        self.ui.rdoutT.setText(str(round(self.TIP[3]*R2D,3)))
        
        """ 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates (done)
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                """ TO DO Here is where affine calibration must be used (done)""" 
                self.whole_aff_matrix = np.append(self.video.aff_matrix,[[0,0,1]],axis = 0)
                Workspace = np.dot(self.whole_aff_matrix,[[x],[y],[1.]])
                self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" % (np.float32(Workspace[0]),np.float32(Workspace[1])))
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
            
        if self.rex.plan_status == 1 :
            self.ui.rdoutStatus.setText("Teaching - Waypoint %d"
                                    %(self.rex.wpt_number + 1))
        elif self.rex.plan_status == 2 :
            self.now = int(time.time()*1000)-self.plan_start            
            if self.poly == 1: # if smooth path was calculated
                self.ui.rdoutStatus.setText("Playing Back - time interval %d"
                                        %(floor(self.now/self.t_wpt)))
                if self.now > self.t_wpt*(self.rex.wpt_total-1):
                    self.rex.plan_status = 3
                    self.poly = 0
                    self.rex.plan = np.delete(self.rex.plan,0, axis = 0)
                    self.rex.wpt_total = self.rex.wpt_total - 1
                    self.rex.plan = self.rex.temp_path
                    self.rex.wpt_total = int((self.rex.wpt_total-1)/3)+1
                    print self.rex.wpt_total, " plan:\n", self.rex.plan*R2D
                else:
                    intv = floor(self.now/self.t_wpt)
                    t_intv = self.now - intv*self.t_wpt
                    for i in range(4):
                        self.rex.speed[i] = abs(self.a1[i][intv] + 2*self.a2[i][intv]*t_intv + 3*self.a3[i][intv]*t_intv**2)*1e3
                        self.rex.joint_angles[i] = self.a0[i][intv] + self.a1[i][intv]*t_intv + self.a2[i][intv]*t_intv**2 + self.a3[i][intv]*t_intv**3
                    self.rex.cmd_publish()
            elif self.fun == 1: # if fun path is on
                """ fun function"""
                fun_points = self.fun_plan()
                #print "fun points ", fun_points, np.sqrt(fun_points[0]**2+fun_points[1]**2+(fun_points[2]-113)**2)
                theta = self.rex.True_IK(fun_points, -20)
                #print theta
                if theta is 0:
                    print 'cannot reach this point'
                else:
                    for i in range(4):
                        self.rex.joint_angles[i] = theta[i]
                    self.rex.cmd_publish()
            else:
                self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                        %(self.rex.wpt_number + 1))
                self.rex.plan_command()
                self.rex.cmd_publish()
            """ Record Feedback """
            self.f_plan_fb.write(str([self.now,\
            	int(self.rex.joint_angles_fb[0]*R2D),\
            	int(self.rex.joint_angles_fb[1]*R2D),\
            	int(self.rex.joint_angles_fb[2]*R2D),\
            	int(self.rex.joint_angles_fb[3]*R2D),\
                int(self.rex.joint_angles[0]*R2D),\
                int(self.rex.joint_angles[1]*R2D),\
                int(self.rex.joint_angles[2]*R2D),\
                int(self.rex.joint_angles[3]*R2D)]))
            self.f_plan_fb.write(";\r")

        elif self.rex.plan_status == 3 :
            """ End execution """
            """ Change Slider """
            self.ui.sldrBase.setProperty("value", int(self.rex.joint_angles[0]*R2D))
            self.ui.sldrShoulder.setProperty("value", int(self.rex.joint_angles[1]*R2D))
            self.ui.sldrElbow.setProperty("value", int(self.rex.joint_angles[2]*R2D))
            self.ui.sldrWrist.setProperty("value", int(self.rex.joint_angles[3]*R2D))
            try:
                self.f_plan_fb.close() # close the feedback file
            except:
                pass
            self.ui.rdoutStatus.setText("Waiting for input")

    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders (done)
        """
        self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
        self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
        self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
        self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R
        self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")
        self.rex.speed = [self.ui.sldrSpeed.value()/100.0 for i in range(4)]
        
        self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration and template defining
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        
        """ If define template is been performed"""
        if (self.defTemp == 1):
            self.defTemp = 2
            self.temp = [[x-MIN_X,y-MIN_Y]]
            self.ui.rdoutStatus.setText("Please click a pair of diagonal points of the minimum rectangular that contains desired template: point 2...")
        elif (self.defTemp == 2):
            self.defTemp = 0
            self.temp.append([x-MIN_X,y-MIN_Y])
            #print 'template', self.temp
            self.ui.rdoutStatus.setText("Waiting for input")
        
        
        
        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y
       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now. (done)
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                
               
                """ Perform affine calibration with OpenCV
                self.video.aff_matrix = cv2.getAffineTransform(
                                        self.video.mouse_coord,
                                        self.video.real_coord)
                print "OpenCV Affine Matrix:\n", self.video.aff_matrix """
                
                """ Perform affine calibration with Marix calculation """
                A = [[self.video.mouse_coord[0][0],self.video.mouse_coord[0][1],1,0,0,0], \
                	[0,0,0,self.video.mouse_coord[0][0],self.video.mouse_coord[0][1],1]]
                for p in range(1,self.video.aff_npoints):
                    A = np.append(A, \
                    	[[self.video.mouse_coord[p][0],self.video.mouse_coord[p][1],1,0,0,0], \
                    	[0,0,0,self.video.mouse_coord[p][0],self.video.mouse_coord[p][1],1]], \
                    	axis = 0)
                b = [[self.video.real_coord[0][0]],[self.video.real_coord[0][1]]]
                for w in range(1,self.video.aff_npoints):
                    b = np.append(b, [[self.video.real_coord[w][0]],[self.video.real_coord[w][1]]], axis = 0)
                
                At = np.transpose(A)
                A = np.dot(At,A)
                b = np.dot(At,b) 
                Ainv = np.linalg.inv(A)
                a2f = np.dot(Ainv,b)
                self.video.aff_matrix = [[a2f[0][0],a2f[1][0],a2f[2][0]],[a2f[3][0],a2f[4][0],a2f[5][0]]]

                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 
                print "Matrix Calculation Affine Matrix:\n", self.video.aff_matrix
                
                xmax = max(self.video.mouse_coord[:,0])
                xmin = min(self.video.mouse_coord[:,0])
                ymax = max(self.video.mouse_coord[:,1])
                ymin = min(self.video.mouse_coord[:,1])
                self.workspace = [xmin,xmax,ymin,ymax]
                #print 'workspace', self.workspace

    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status textlabel 
        """
        print "Affine Calibration"
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        self.video.loadCalibration()
        print "Load Camera Cal"

    def tr_initialize(self):
        """ start Teaching """
        self.rex.plan_status = 1
        print "Teach and Repeat"
        self.rex.plan = [[self.rex.joint_angles_fb[0],self.rex.joint_angles_fb[1],self.rex.joint_angles_fb[2],self.rex.joint_angles_fb[3]]]
        self.rex.wpt_number = 1
        print "Start position: "
        print "B: ", self.rex.plan[0][0]*R2D
        print "S: ", self.rex.plan[0][1]*R2D
        print "E: ", self.rex.plan[0][2]*R2D
        print "W: ", self.rex.plan[0][3]*R2D
        self.ui.sldrMaxTorque.setProperty("value", 0)
        

    def tr_add_waypoint(self):
        if self.rex.plan_status == 1:
        	print "Add Waypoint ", self.rex.wpt_number
        	print "B: ", self.rex.joint_angles_fb[0]*R2D
        	print "S: ", self.rex.joint_angles_fb[1]*R2D
        	print "E: ", self.rex.joint_angles_fb[2]*R2D
        	print "W: ", self.rex.joint_angles_fb[3]*R2D
        	self.rex.plan = np.append(self.rex.plan, [[self.rex.joint_angles_fb[0],self.rex.joint_angles_fb[1],self.rex.joint_angles_fb[2],self.rex.joint_angles_fb[3]]], axis = 0)
        	self.rex.wpt_number = self.rex.wpt_number + 1
        	self.rex.wpt_total = self.rex.wpt_number
        else:
            print "Teach and Repeat not started."
	

    def tr_smooth_path(self):
        if self.rex.plan_status != 0:
            """ Rise a little by specific angles
            self.rex.temp_path = self.rex.plan
            
            Shoulder_offset = 20*D2R
            Wrist_offset = 5*D2R
            smooth_path = [[self.rex.plan[0][0],self.rex.plan[0][1],self.rex.plan[0][2],self.rex.plan[0][3]]]
            for wpt in range(1, self.rex.wpt_total):
            	smooth_path = np.append(smooth_path,\
            		 [[self.rex.plan[wpt][0],\
            		 self.rex.plan[wpt][1]-np.sign(self.rex.plan[wpt][1])*Shoulder_offset,\
            		 self.rex.plan[wpt][2],\
            		 self.rex.plan[wpt][3]+np.sign(self.rex.plan[wpt][3])*Wrist_offset]], axis = 0)
            	smooth_path = np.append(smooth_path, [[self.rex.plan[wpt][0],self.rex.plan[wpt][1],self.rex.plan[wpt][2],self.rex.plan[wpt][3]]], axis = 0)
            	smooth_path = np.append(smooth_path,\
            		 [[self.rex.plan[wpt][0],\
            		 self.rex.plan[wpt][1]-np.sign(self.rex.plan[wpt][1])*Shoulder_offset,\
            		 self.rex.plan[wpt][2],\
            		 self.rex.plan[wpt][3]+np.sign(self.rex.plan[wpt][3])*Wrist_offset]], axis = 0)
            
            self.rex.plan = smooth_path
            self.rex.wpt_total = (self.rex.wpt_total-1)*3+1
            self.rex.wpt_number = self.rex.wpt_total
            self.rex.plan_status = 3 """
            
            """ Cube Polynomial """
            zh = 50 # rise height
            """ Use Inverse Kinematics to calculate rising angle """
            smooth_path = [[self.rex.plan[0][0], self.rex.plan[0][1],\
                self.rex.plan[0][2], self.rex.plan[0][3]]]
            for wpt in range(1,self.rex.wpt_total):
                pos = self.rex.rexarm_FK(self.rex.plan[wpt])
                pos_rise = pos
                pos_rise[2] = pos_rise[2]+zh
                theta_rise = self.rex.True_IK(pos_rise,0)
                smooth_path = np.append(smooth_path, [[theta_rise[0],theta_rise[1],theta_rise[2],theta_rise[3]]], axis = 0)
                smooth_path = np.append(smooth_path, [[self.rex.plan[wpt][0],self.rex.plan[wpt][1],self.rex.plan[wpt][2],self.rex.plan[wpt][3]]], axis = 0)
                smooth_path = np.append(smooth_path, [[theta_rise[0],theta_rise[1],theta_rise[2],theta_rise[3]]], axis = 0)

            self.rex.plan = smooth_path
            self.rex.wpt_total = (self.rex.wpt_total-1)*3+1
            self.rex.wpt_number = self.rex.wpt_total

            """ calculate polynomial parameters """
            self.rex.plan = np.append([[self.rex.joint_angles_fb[0],self.rex.joint_angles_fb[1],self.rex.joint_angles_fb[2],self.rex.joint_angles_fb[3]]], self.rex.plan, axis = 0)
            self.rex.wpt_total = self.rex.wpt_total + 1
            print self.rex.wpt_total, "smooth plan:\n", self.rex.plan*R2D
            v0 = 0 	# speed at start and end
            vn = 0 	# speed at middle
            self.t_wpt = 1500 ; # time between waypoints (ms)
            self.a0 = [[] for i in range(4)]
            self.a1 = [[] for i in range(4)]
            self.a2 = [[] for i in range(4)]
            self.a3 = [[] for i in range(4)]
            for i in range(4):
            	q0 = self.rex.plan[0][i]
            	q1 = self.rex.plan[1][i]
            	self.a0[i] = [q0]
            	self.a1[i] = [v0]
            	self.a2[i] = [(3*(q1-q0)-(2*v0+vn)*self.t_wpt)/self.t_wpt**2]
            	self.a3[i] = [(2*(q0-q1)+(v0+vn)*self.t_wpt)/self.t_wpt**3]
            for wpt in range(1,self.rex.wpt_total-2):
            	for i in range(4):
            		q0 = self.rex.plan[wpt][i]
            		q1 = self.rex.plan[wpt+1][i]
            		self.a0[i] = np.append(self.a0[i],[q0])
            		self.a1[i] = np.append(self.a1[i],[vn])
            		self.a2[i] = np.append(self.a2[i],[(3*(q1-q0)-(2*vn+vn)*self.t_wpt)/self.t_wpt**2])
            		self.a3[i] = np.append(self.a3[i],[(2*(q0-q1)+(vn+vn)*self.t_wpt)/self.t_wpt**3  ])
            for i in range(4):
            	q0 = self.rex.plan[-2][i]
            	q1 = self.rex.plan[-1][i]
            	self.a0[i] = np.append(self.a0[i],[q0])
            	self.a1[i] = np.append(self.a1[i],[vn])
            	self.a2[i] = np.append(self.a2[i],[(3*(q1-q0)-(2*vn+v0)*self.t_wpt)/self.t_wpt**2])
            	self.a3[i] = np.append(self.a3[i],[(2*(q0-q1)+(vn+v0)*self.t_wpt)/self.t_wpt**3  ])
            self.poly = 1

            """ plot polynimials
            plt.figure(figsize=(15,10),dpi=80)
            t = np.arange(3000)
            for j in range(4):
            	for i in range(self.rex.wpt_total-1):
            		plt.subplot(2,2,j+1)
            		plt.plot(t+i*self.t_wpt, (self.a0[j][i] + self.a1[j][i]*t + self.a2[j][i]*t**2 + self.a3[j][i]*t**3)*R2D)
            plt.show()
            """

            """ print a0, a1, a2, a3 for 4 joints
            for j in range(4):
                for i in range(self.rex.wpt_total-1):
                    print "\njoint", j, "time", i, "a0: ", self.a0[j][i], "a1: ", self.a1[j][i], "a2: ", self.a2[j][i], "a3: ", self.a3[j][i], 
                    #print "joint", j, " command", i, (self.a0[j][i] + self.a1[j][i]*self.t_wpt + self.a2[j][i]*self.t_wpt**2 + self.a3[j][i]*self.t_wpt**3)*R2D
            """
        else:
            print "No plan"

    def tr_playback(self):
    	if self.rex.plan_status != 0:
    		print "Save ", self.rex.wpt_total, " plan:\n", self.rex.plan*R2D
    		self.rex.plan_status = 3
    		np.savez('plan.npz', points = np.array(self.rex.plan), number = self.rex.wpt_total)
    	else:
    		print "No plan"

    def load_plan(self):
    	plan = np.load('plan.npz')
    	self.rex.plan = plan['points']
    	self.rex.wpt_total = plan['number']
    	self.rex.wpt_number = plan['number']
    	self.rex.plan_status = 3
    	print self.rex.wpt_total, "Load Plan:\n", self.rex.plan*R2D

    def def_template(self):
        print "Define Template"
        self.defTemp = 1
        self.temp = []
        self.ui.rdoutStatus.setText("Please click a pair of diagonal points of the minimum rectangular that contains desired template: point 1...")
                

    def template_match(self):
        print "Template Match"
        currentFrame = self.video.currentFrame
        
        # cut out parts of workspace and template images from current frame, and change RGB to GREY
        self.img = currentFrame[2*self.workspace[2]:2*self.workspace[3],2*self.workspace[0]:2*self.workspace[1],:]
        self.img = 0.299*self.img[:,:,0]+0.587*self.img[:,:,1]+0.114*self.img[:,:,2]
        tminx = self.temp[0][0]
        tmaxx = self.temp[1][0]
        tminy = self.temp[0][1]
        tmaxy = self.temp[1][1]
        if (tminx > tmaxx):
            tminx = self.temp[1][0]
            tmaxx = self.temp[0][0]
        if (tminy > tmaxy):
            tminy = self.temp[1][1]
            tmaxy = self.temp[0][1]
        self.tempimg = currentFrame[2*tminy:2*tmaxy, 2*tminx:2*tmaxx, :]
        self.tempimg = 0.299*self.tempimg[:,:,0]+0.587*self.tempimg[:,:,1]+0.114*self.tempimg[:,:,2]
        tempimgr = 2*tmaxy-2*tminy
        tempimgc = 2*tmaxx-2*tminx
        imgr = 2*self.workspace[3]-2*self.workspace[2]
        imgc = 2*self.workspace[1]-2*self.workspace[0]
        

        t = time.time()
        
        # sample images
        mini = 10 # sample factor
        self.imgsam = np.zeros([int(imgr/mini),int(imgc/mini)])
        for i in range(int(imgr/mini)):
            for j in range(int(imgc/mini)):
                self.imgsam[i,j] = self.img[i*mini,j*mini]
        self.tempimgsam = np.zeros([int(tempimgr/mini),int(tempimgc/mini)])
        for i in range(int(tempimgr/mini)):
            for j in range(int(tempimgc/mini)):
                self.tempimgsam[i,j] = self.tempimg[i*mini,j*mini]

        # template detection
        lis = np.zeros([(imgr-tempimgr)/mini,(imgc-tempimgc)/mini])
        for i in range(int((imgr-tempimgr)/mini)):
            for j in range(int((imgc-tempimgc)/mini)):
                coor = self.imgsam[i:i+tempimgr/mini,j:j+tempimgc/mini]-self.tempimgsam[:,:]
                coor = coor ** 2
                coor = np.sum(coor, dtype = int)
                lis[i,j]=coor
        index = np.where(lis<=(1*lis.min()+1*lis.std()))
        indexw = np.array(zip(index[0],index[1]))*mini+[tempimgr/2,tempimgc/2]
        



        ## targets merge
        sh = np.shape(indexw)
        n = int(sh[0])
        target = [indexw[0,:]]
        k = 1
        flag = 1
        for i in range(n):
            j = 0
            flag = 1
            while j < k:
               if sum(abs(indexw[i,:]-target[j])) < max(tempimgr,tempimgc)*1:
                    flag = 0
                    break
               j = j+1
            if (flag == 1): 
                target = np.append(target,[indexw[i,:]],axis = 0)
                k = k + 1

        ## delete fake target(arm base) if have one
        target = list(target)
        for i in range(k):
            if sum(abs(target[i]-[imgr/2,imgc/2])) <  max(tempimgr,tempimgc)*1:
                target.pop(i)
                break
        target = np.array(target)

        
        
        sh = np.shape(target)
        m = int(sh[0])  # save number of targets detected
        
        ##'calibrate' target
        for i in range(m):
            sumr = 0
            sumc = 0
            num = 0
            for j in range(n):
                if sum(abs(indexw[j,:]-target[i])) < max(tempimgr,tempimgc):
                    sumr = sumr + indexw[j,0]
                    sumc = sumc + indexw[j,1]
                    num = num+1
            target[i] = [sumr/num,sumc/num]

        #self.temp_p = (target + np.array([self.video.mouse_coord[1][1],self.video.mouse_coord[1][0]])*2-np.array([4,16])).tolist()
        self.temp_p = (target + np.array([self.workspace[2],self.workspace[0]])*2).tolist()


        ## place targets to arm frame
        temp_p = np.transpose(self.temp_p)/2
        temp_p = np.append([temp_p[1,:]],[temp_p[0,:]],axis = 0)
        temp_p = np.append(temp_p,[np.ones(m)],axis = 0)
        tar = np.dot(self.whole_aff_matrix,temp_p)
        tar = np.transpose(tar)
        tar = tar[:,0:2]
        print tar
        invalid = 0
        for i in range(m):
            if abs(tar[i,0])>=300 or abs(tar[i,1])>=300:
                invalid = invalid + 1
        realtar = np.zeros([m-invalid,2],dtype = np.int64)
        #print m, invalid
        j = 0
        for i in range(m):
            if abs(tar[i,0])<300 and abs(tar[i,1])<300:
                realtar[j] = tar[i]
                j = j+1
        m = m-invalid
        tar = realtar
        
        # sort targets
        self.template_target = np.zeros([m,3],dtype=np.int64)
        zh = 5
        poswp = []
        negwp = []
        for i in range(m):
            self.template_target[i,0] = tar[i,0]
            self.template_target[i,1] = tar[i,1]
            self.template_target[i,2] = zh
            if tar[i,0]<0:
                negwp.append(self.template_target[i])
            else:
                poswp.append(self.template_target[i])
        poswp = np.array(poswp)
        negwp = np.array(negwp)
        poswp = sorted(poswp,key = lambda tup: tup[1])
        negwp = sorted(negwp,key = lambda tup: tup[1])
        shn = np.shape(negwp)
        shn = int(shn[0])
        shp = np.shape(poswp)
        shp = int(shp[0])
        if ((shn is not 0) and (shp is not 0)):
            self.template_target = np.append(poswp,negwp,axis = 0)
        if shn is 0:
            self.template_target = poswp
        if shp is 0:
            self.template_target = negwp



        print 'template matching time: ', time.time() - t




        '''
        #uncomment to plot workspace image, template image and result of detection 
        fig,(F1,F2,F3) = plt.subplots(1,3)
        F1.imshow(self.img)
        F2.imshow(self.tempimg)
        #F2.plot(indexw[:,0],indexw[:,1],'*')  # raw result of template detection
        F3.plot(-self.template_target[:,0],-self.template_target[:,1],'*')
        fig.show()
        '''
        


        ##generate points that mark templates in GUI

        #print 'Found Template: \n', self.template_target
        tar = np.transpose(tar)
        tar = np.append(tar,[np.ones(m)],axis = 0)
        self.printpoints = np.dot(np.linalg.inv(self.whole_aff_matrix),tar)
        self.printpoints = self.printpoints[0:2,:]
        self.printpoints = np.append([self.printpoints[1,:]],[self.printpoints[0,:]],axis = 0)
        self.printpoints = np.transpose(self.printpoints)*2
        #print 'tar',tar
        #print 'ppoints',self.printpoints
        
        self.rex.plan = [[0,0,0,0]]
        self.rex.wpt_total = m+1
        for i in range(m):
            theta = self.rex.True_IK(self.template_target[i],0)
            #print theta
            if theta is 0:
                print 'cannot reach this point ', self.template_target[i]
                self.rex.wpt_total = self.rex.wpt_total -1
            else:
                self.rex.plan = np.append(self.rex.plan, [theta], axis = 0)
        self.rex.wpt_number = self.rex.wpt_total
        self.rex.plan_status = 3
        print self.rex.wpt_total, " plan:\n", self.rex.plan*R2D

    def exec_path(self):
    	if self.rex.plan_status != 0:
    		self.rex.wpt_number = 0
    		self.rex.plan_status = 2
    		self.ui.sldrMaxTorque.setProperty("value", 50)
    		self.ui.sldrSpeed.setProperty("value", 10)
    		print "Execute Path"
    		print self.rex.wpt_total, " plan:\n", self.rex.plan*R2D
    		self.f_plan_fb = open('plan_feedback.cfg', 'a')
    		self.f_plan_fb.write("\rTime(ms), Base, Shoulder, Elbow, Wrist(Deg) (feedback/command)\r")
    		self.plan_start = int(time.time()*1000)
    	elif self.fun == 1:
    		self.rex.plan_status = 2
    		self.ui.sldrMaxTorque.setProperty("value", 70)
    		self.ui.sldrSpeed.setProperty("value", 50)
    		print "Execute Fun Path"
    		self.f_plan_fb = open('plan_feedback.cfg', 'a')
    		self.f_plan_fb.write("\rTime(ms), Base, Shoulder, Elbow, Wrist(Deg)\r")
    		self.plan_start = int(time.time()*1000)
    	else:
    		print "No plan"
    """ fun function"""
    def fun_plan(self):
    	ang = 2*1e-4*pi*self.now
    	r = 70-5*sin(ang)+25*sin(3*ang)+20*sin(5*ang)-17*sin(7*ang)+30*cos(2*ang)-20*cos(4*ang)-4*cos(16*ang)
    	r = r*1.2
        y = r*cos(ang)
        x = 100 + abs(y)*0.5
    	z = 200+r*sin(ang)
    	return [x,y,z]
 
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
