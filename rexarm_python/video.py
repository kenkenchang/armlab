import cv2
import numpy as np
import os
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.w = 1280
        self.h = 960
        self.capture.set(3, self.w)
        self.capture.set(4, self.h)
        self.currentFrame=np.array([])
        self.cal_flag = False

        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 5
        self.real_coord = np.float32([[0., 0.], [300.,-300.],[-300.,-280.],[-280.,290.],[300.,280.]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0,0.0],[0.0,0.0]]) 
        #self.aff_npoints = 3
        #self.real_coord = np.float32([[0., 0.], [300.,-300.],[-300.,-280.]])
        #self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])      
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32((2,3))
    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret==True):
       	    if self.cal_flag == True:
       	    	new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.calibration['CM'],self.calibration['DC'],(self.w,self.h),1,(self.w,self.h))
        	rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
       	    	self.currentFrame= cv2.undistort(rgb_frame, self.calibration['CM'], self.calibration['DC'], None, new_camera_matrix)
       	    else:
            	self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)

    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """
        
        self.calibration = np.load('calibration.npz')
        if os.path.getsize("calibration.npz") != 0:
            self.cal_flag = True
        else:
            self.cal_flag = False
    def drawSquare(self, pos):
        """
        Show red square on Frame at pos(pixel)
        """
        square_l = 10
        l = len(pos)
        for p in range(l):
            if pos[p][0] > self.h-square_l or pos[p][1] > self.w-square_l:
                print "Position ", pos[p], " beyond frame"
            else:
                for i in range(square_l):
                    for j in range(square_l):
                        self.currentFrame[pos[p][0]+i][pos[p][1]+j][0] = 255.
                        self.currentFrame[pos[p][0]+i][pos[p][1]+j][1] = 0.
                        self.currentFrame[pos[p][0]+i][pos[p][1]+j][2] = 0.
                
