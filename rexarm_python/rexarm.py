import lcm
import time
import numpy as np
import math

from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t

PI = np.pi
D2R = PI/180.0
R2D = 180.0/PI
ANGLE_TOL = 2*PI/180.0 


""" Rexarm Class """
class Rexarm():
    def __init__(self):

        """ Commanded Values """
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians
        # you SHOULD change this to contorl each joint speed separately 
        self.speed = [0.5,0.5,0.5,0.5]           # 0 to 1
        self.max_torque = 0.5                    # 0 to 1

        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius               

        """ Plan """
        self.plan = []
        self.temp_plan = []
        self.plan_status = 0	# 0: no plan; 1: recording plan; 2: play plan; 3: plan saved
        self.wpt_number = 0
        self.wpt_total = 0
        self.time_plan = 0      # command plan based on time

        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)

    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        msg = dynamixel_command_list_t()
        msg.len = 4
        self.clamp()
        for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            cmd.position_radians = self.joint_angles[i]
            # you SHOULD change this to contorl each joint speed separately 
            cmd.speed = self.speed[i]
            cmd.max_torque = self.max_torque
            # print cmd.position_radians
            msg.commands.append(cmd)
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians 
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature

    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        TO DO: IMPLEMENT SUCH FUNCTION (done)
        """
        
        self.Smax = 125*D2R
        self.Smin = -125*D2R
        self.Emax = 120*D2R
        self.Emin = -120*D2R
        self.Wmax = 125*D2R
        self.Wmin = -125*D2R
        
        if self.joint_angles[1] > self.Smax:
        	self.joint_angles[1] = self.Smax
        if self.joint_angles[1] < self.Smin:
        	self.joint_angles[1] = self.Smin
        if self.joint_angles[2] > self.Emax:
        	self.joint_angles[2] = self.Emax
        if self.joint_angles[2] < self.Emin:
        	self.joint_angles[2] = self.Emin
        if self.joint_angles[3] > self.Wmax:
        	self.joint_angles[3] = self.Wmax
        if self.joint_angles[3] < self.Wmin:
        	self.joint_angles[3] = self.Wmin
        
        

    def plan_command(self):
    	""" Command waypoints """
    	self.joint_angles[0] = self.plan[self.wpt_number][0]
    	self.joint_angles[1] = self.plan[self.wpt_number][1]
    	self.joint_angles[2] = self.plan[self.wpt_number][2]
    	self.joint_angles[3] = self.plan[self.wpt_number][3]
    	error = (self.joint_angles[0]- self.joint_angles_fb[0])**2 + \
    			(self.joint_angles[1]- self.joint_angles_fb[1])**2 + \
    			(self.joint_angles[2]- self.joint_angles_fb[2])**2 + \
    			(self.joint_angles[3]- self.joint_angles_fb[3])**2
    	error_thres = 4 # error sum of four motors in degree
    	if error < (error_thres*D2R)**2:
    		if self.wpt_number < self.wpt_total-1 :
    			self.wpt_number = self.wpt_number + 1
    			self.joint_angles[0] = self.plan[self.wpt_number][0]
    			self.joint_angles[1] = self.plan[self.wpt_number][1]
    			self.joint_angles[2] = self.plan[self.wpt_number][2]
    			self.joint_angles[3] = self.plan[self.wpt_number][3]
    		else:
    			self.wpt_number = self.wpt_number + 1
    			self.plan_status = 3
        
    def generate_A(self,a,alpha,d,theta):
        """
        Generate and Return homogeneous transformation matrix A for forward kinematics
        """
        A = np.array([[np.cos(theta),   -np.sin(theta)*np.cos(alpha),   np.sin(theta)*np.sin(alpha),   a*np.cos(theta)],
                      [np.sin(theta),   np.cos(theta)*np.cos(alpha),   -np.cos(theta)*np.sin(alpha),   a*np.sin(theta)],
                      [         0,              np.sin(alpha),               np.cos(alpha),              d],
                      [         0,                       0,                        0,              1]])
        return A
            
    def rexarm_FK(self,theta):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        
        This function has two mode. 
        If theta is -1, the function is used to calculate tip position that is shown on GUI.
        If theta is a 4 by 1 vector, the function is used for path smoothing calculation.
        """
        
        
        if (theta is -1):
        	DH = [[0,PI/2,116,self.joint_angles[0]],[100,0,0,PI/2+self.joint_angles[1]],[100,0,0,self.joint_angles[2]],[110,0,0,self.joint_angles[3]]]
	else:
		DH = [[0,PI/2,116,theta[0]],[100,0,0,PI/2+theta[1]],[100,0,0,theta[2]],[110,0,0,theta[3]]]
        A1 = self.generate_A(DH[0][0],DH[0][1],DH[0][2],DH[0][3])
        A2 = self.generate_A(DH[1][0],DH[1][1],DH[1][2],DH[1][3])
        A3 = self.generate_A(DH[2][0],DH[2][1],DH[2][2],DH[2][3])
        A4 = self.generate_A(DH[3][0],DH[3][1],DH[3][2],DH[3][3])
        tip = np.dot(A1,np.dot(A2,np.dot(A3,np.dot(A4,np.array([[0],[0],[0],[1]])))))
        if (theta is -1):
        	TIP = [tip[0][0],tip[1][0],tip[2][0],abs(self.joint_angles[1]+self.joint_angles[2]+self.joint_angles[3])-PI/2]
        else:
        	TIP = [tip[0][0],tip[1][0],tip[2][0],abs(theta[1]+theta[2]+theta[3])-PI/2]
        return TIP
        

    def True_IK(self, pose, p):
        """
        Search and return an feasible solution of inversise kinametic (IK) 
        since phi is usually unknown and IK usually has multiple solutions.
        
        0 is returned if no feasible solution is found.
        
        Pose: desired tip position.
        p: starting search angle of phi. If p is 0, phi starts from -90 deg. 
           Otherwise phi starts from p (deg).
        """
        
        if p ==0:
            p = -90  
        theta = -1
        for phi in range(p,p+360):    
            theta = self.rexarm_IK(pose,phi*D2R)
            if theta is -2:
                break
                return 0
            if theta is not -1:
                return theta
                break
        return 0
    	
    def rexarm_IK(self,pose,phi):
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        
        rexarm_IK retrns:
        -2: input tip position invalid
        -1: collision happens with current phi
        theta: a feasible joint angle commend
        """
        
        x = pose[0]
        y = pose[1]
        z = pose[2]
        #f = 0  # flag used to check wrist position status
        
        #Check if the input position is valid.
        if (z<0):
            print 'z<0!!!! Pose invalid'
            return -2
        if x**2+y**2<25**2 and z<116+15:  
            print 'tip hits base!!!! Pose invalid'
            return -2
        tipdis = x**2+y**2+(z-116)**2
        if tipdis>=310**2+5:
	    print 'tip outside workspace!!!! Pose invalid'
	    return -2

        # Unify phi in -PI to PI
        while(phi>PI):
            phi = phi-2*PI
        while(phi<-PI):
            phi = phi+2*PI
        
    
        # Calculate desired base angle and force it to be in -90 [deg] to 90 [deg]
        r = np.sqrt(x**2+y**2)
        s = z-116    
        t0 = math.atan2(y,x) 
        if (t0*R2D<-90):
            t0 = t0+PI
        if (t0*R2D>90):
            t0 = t0-PI
        
        #Calculating desired wrist position
        rp = r-110*np.cos(phi)
        sp = s+110*np.sin(phi)
        
        # Special case: check if desired position is on the furthest reachable points of arm
        if abs(x**2+y**2+(z-116)**2-310**2)<5:
            t2 = 0
            t3 = 0
            t1 = -np.sign(x)*(PI/2-np.arctan((z-116)/np.sqrt(x**2+y**2)))
            return [t0,t1,t2,t3]   
        
        # check if desired wrist position is outside its reachable area 
        if (rp**2+sp**2>200**2): 
            return -1
    
        # Calculate other joint angles
        temp = (100**2+100**2-rp**2-sp**2)/(2*100*100)    
        if abs(temp)>1 and abs(temp)-1<0.00001:
            temp = np.sign(temp)    
        t2 = PI-np.arccos(temp)

        temp = (rp**2+sp**2)/(2*100*np.sqrt(rp**2+sp**2))    
        if abs(temp)>1 and abs(temp)-1<0.00001:
            temp = np.sign(temp)
        
        if (rp>0):
            t1 = PI/2-np.arctan(sp/rp)-np.arccos(temp)
        elif (sp>0):
            t1 = np.arctan(rp/sp)-np.arccos(temp)
        else:
            t1 = PI - np.arctan(rp/sp)-np.arccos(temp)    

        t3 = PI/2+phi-t1-t2     

    
        # construct theta
        if(x>0):
            theta = [t0,-t1,-t2,-t3]
        else:
            theta = [t0,t1,t2,t3]
    
        for i in range(4):
            while theta[i]>PI:
                theta[i] = theta[i]-2*PI
            while theta[i]<-PI:
                theta[i] = theta[i]+2*PI
            
            
        #Collision check for physical structure
        if abs(theta[1])>125*D2R or abs(theta[2])>120*D2R or abs(theta[3])>125*D2R:
            #print 'angle commend larger than maximum with current phi!!!!'
            return -1
        
        a = 10 #calculation threshold
        if 100**2+100**2-2*100*100*np.cos(PI-abs(theta[2])) < (110+a)**2:
            if np.sign(theta[3])==np.sign(theta[2]) and abs(theta[3]) >= PI - abs(theta[2])+np.arccos((110+a)**2/(2*100*(110+a))):
               # print 'tip will hit sholder!!!!'
                return -1
        
        if 116**2+100**2-2*116*100*np.cos(PI-abs(theta[1])) < (100+a)**2:
            if np.sign(theta[2])==np.sign(theta[1]) and abs(theta[2]) >= PI - abs(theta[1])+np.arccos((116**2+(100+a)**2-100**2)/(2*116*(100+a))):
                #print 'wrist will hit base!!!!'
                return -1
     
        DH = [[0,PI/2,116,theta[0]],[100,0,0,PI/2+theta[1]],[100,0,0,theta[2]],[110,0,0,theta[3]]]
        A1 = self.generate_A(DH[0][0],DH[0][1],DH[0][2],DH[0][3])
        A2 = self.generate_A(DH[1][0],DH[1][1],DH[1][2],DH[1][3])
        A3 = self.generate_A(DH[2][0],DH[2][1],DH[2][2],DH[2][3])
        w = np.dot(A1,np.dot(A2,np.dot(A3,np.array([[0],[0],[0],[1]]))))
        if w[0][0]**2+w[1][0]**2+w[2][0]**2<=(110+a)**2:
            if np.sign(theta[3])==np.sign(theta[1]) and abs(theta[3])>=PI+np.arctan(abs(w[2][0]/np.sqrt(w[1][0]**2+w[0][0]**2)))-abs(theta[1])-abs(theta[2]):
               # print 'tip will hit base!!!!'
                return -1
        if w[2][0]<=90:
            #print 'wrist too low, may hit templates'
            return -1
        
    
        return theta  
        
        
        
    def rexarm_collision_check(q):
        """
        Perform a collision check with the ground and the base
        takes a 4-tuple of joint angles q
        returns true if no collision occurs
        """
        
        # IK includes collision check, so we remain this function as blank
        pass 
