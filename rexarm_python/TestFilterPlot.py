import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

R2D = 180.0/3.141592

plan = np.load('plan.npz')
rex_plan = plan['points']
rex_wpt_total = plan['number']
#print rex_wpt_total, "Load Plan:\n", rex_plan*R2D

fs = 10
fl = 0.4
order = 3
plt.figure(figsize=(15,10),dpi=80)
smooth_path = [[rex_plan[0][0], rex_plan[0][1], rex_plan[0][2],rex_plan[0][3]]]
for wpt in range(1,rex_wpt_total):
    interval = (rex_plan[wpt] - rex_plan[wpt-1])/fs
    for k in range(fs):
        smooth_path = np.append(smooth_path,[smooth_path[-1]+interval], axis = 0)

x = range((rex_wpt_total-1)*fs+1)
for joint in range(4):
    plto = []
    for i in x:
        plto = np.append(plto,[smooth_path[i][joint]*R2D],axis = 0)
    plt.subplot(2,2,joint+1)
    plt.plot(plto,color='blue',linestyle='--')
    #plt.xlim(0,5)
    #plt.ylim(-0.1,0.1)

b, a = signal.butter(order,fl,'low')
smooth_path = signal.lfilter(b,a,smooth_path)

for joint in range(4):
    pltf = []
    for k in x:
        pltf = np.append(pltf,[smooth_path[k][joint]],axis = 0)
    plt.subplot(2,2,joint+1)
    plt.plot(pltf,color='red',linestyle = '--')
    #plt.xlim(0,3)
    #plt.ylim(-0.1,0.1)

plt.show()
"""
# test

x = np.linspace(-np.pi*2, np.pi*2, 256, endpoint=True)
c1 = np.sin(x)
c2 = np.sin(np.dot(x,2))
c = c1+c2
plt.subplot(1,1,1)
plt.plot(c,color = 'blue')

order = 5
fl = 0.05
b, a = signal.butter(order,fl,'low')
cf = signal.lfilter(b,a,c)
plt.subplot(1,1,1)
plt.plot(cf,color = 'red')

plt.show()
"""
